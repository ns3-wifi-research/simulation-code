/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2019 Rajarishi Devarajan
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Rajarishi Devarajan <rishi93dev@gmail.com>
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("MovingDownlinkPlus");

class AppTxTag : public Tag{
    private:
    Time m_timestamp;

    public:
    static TypeId GetTypeId(void){
        static TypeId tid = TypeId("AppTxTag")
        .SetParent<Tag>()
        .AddConstructor<AppTxTag>()
        .AddAttribute("Timestamp", "A simple timestamp", EmptyAttributeValue(), MakeTimeAccessor(&AppTxTag::GetTimestamp), MakeTimeChecker());
        return tid;
    }

    virtual TypeId GetInstanceTypeId(void) const{
        return GetTypeId();
    }

    virtual uint32_t GetSerializedSize(void) const{
        return 8;
    }

    virtual void Serialize(TagBuffer i) const{
        int64_t t = m_timestamp.GetNanoSeconds();
        i.Write((const uint8_t *)&t, 8);
    }

    virtual void Deserialize(TagBuffer i){
        int64_t t;
        i.Read((uint8_t *)&t, 8);
        m_timestamp = NanoSeconds(t);
    }

    void Print(std::ostream &os) const{
        os <<"t=" << m_timestamp;
    }

    void SetTimestamp(Time time){
        m_timestamp = time;
    }

    Time GetTimestamp(void) const{
        return m_timestamp;
    }
};

void AppTx(Ptr<const Packet> p){
    AppTxTag timestamp;
    timestamp.SetTimestamp(Simulator::Now());
    p->AddPacketTag(timestamp);
}

void AppRx(Ptr<const Packet> p, const Address &address){
    Time app_rx_timestamp = Simulator::Now();
    AppTxTag app_tx_timestamp;
    p->PeekPacketTag(app_tx_timestamp);
    NS_LOG_INFO(app_tx_timestamp.GetTimestamp() << "," << app_rx_timestamp);
}

int main(int argc, char *argv[]){
    /* Set time resolution to nanoseconds */
    Time::SetResolution(Time::NS);

    /* Enable log messages */
    LogComponentEnable("MovingDownlinkPlus", LOG_INFO); 

    /* Enable checksums */
    GlobalValue::Bind("ChecksumEnabled", BooleanValue(true));

    /* Create the Nodes */
    NS_LOG_INFO("Creating the Nodes...");
    NodeContainer wifiApNodes;
    wifiApNodes.Create(1);
    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(1);

    /* Create the wifi channel */
    NS_LOG_INFO("Setting up the channel...");
    WifiHelper wifi;
    wifi.SetStandard(WIFI_PHY_STANDARD_80211n_2_4GHZ);
    wifi.SetRemoteStationManager("ns3::MinstrelHtPlusWifiManager");

    /* Setup the PHY */
    YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
    //wifiPhy.DisablePreambleDetectionModel(); // Disable to receive even when < -82dBm
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();

    /* Setup the Mobility */
    MobilityHelper apMobility;
    apMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    Ptr<ListPositionAllocator> apPositionAlloc = CreateObject<ListPositionAllocator>();
    apPositionAlloc->Add(Vector(0.0, 0.0, 1.0));
    apMobility.SetPositionAllocator(apPositionAlloc);
    apMobility.Install(wifiApNodes);

    MobilityHelper staMobility;
    staMobility.SetMobilityModel("ns3::ConstantVelocityMobilityModel");
    Ptr<ListPositionAllocator> staPositionAlloc = CreateObject<ListPositionAllocator>();
    staPositionAlloc->Add(Vector(5.0, 0.0, 1.0));
    staMobility.SetPositionAllocator(staPositionAlloc);
    staMobility.Install(wifiStaNodes);
    Ptr<ConstantVelocityMobilityModel> staVelocity = wifiStaNodes.Get(0)->GetObject<ConstantVelocityMobilityModel>();
    staVelocity->SetVelocity(Vector(1.0, 0.0, 0.0));

    wifiPhy.SetChannel(wifiChannel.Create());

    /* Setup the MAC */
    WifiMacHelper wifiMac;
    Ssid ssid = Ssid("ns3");

    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiApDevices = wifi.Install(wifiPhy, wifiMac, wifiApNodes);

    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiStaDevices = wifi.Install(wifiPhy, wifiMac, wifiStaNodes);

    /* Install the internet stack */
    NS_LOG_INFO("Installing the internet stack...");
    InternetStackHelper internet;
    internet.Install(wifiApNodes);
    internet.Install(wifiStaNodes);

    /* Assign the IP addresses */
    NS_LOG_INFO("Assigning the IP addresses...");
    Ipv4AddressHelper address;
    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiApInterfaces = address.Assign(wifiApDevices);
    Ipv4InterfaceContainer wifiStaInterfaces = address.Assign(wifiStaDevices);

    /* Setup the applications */
    NS_LOG_INFO("Install the applications...");
    uint16_t port = 9;
    PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    ApplicationContainer apps_sink = sink.Install(wifiStaNodes.Get(0));
    apps_sink.Start(Seconds(0.5));
    apps_sink.Stop(Seconds(100.0));

    OnOffHelper onoff("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    onoff.SetConstantRate(DataRate("400Mb/s"), 1420);
    onoff.SetAttribute("StartTime", TimeValue(Seconds(0.5)));
    onoff.SetAttribute("StopTime", TimeValue(Seconds(100.0)));
    ApplicationContainer apps_source = onoff.Install(wifiApNodes.Get(0));

    /* Connect the callbacks */
    Config::ConnectWithoutContext("/NodeList/*/ApplicationList/*/$ns3::OnOffApplication/Tx", MakeCallback(&AppTx));
    Config::ConnectWithoutContext("/NodeList/*/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback(&AppRx));

    /* Set PCAP attributes */
    //Config::SetDefault("ns3::PcapFileWrapper:NanosecMode", BooleanValue(true));
    wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
    wifiPhy.EnablePcap("ap", wifiApDevices.Get(0));
    wifiPhy.EnablePcap("sta", wifiStaDevices.Get(0));

    /* Start simulation */
    NS_LOG_INFO("Running simulation...");
    Simulator::Stop(Seconds(100.0));
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Finished simulation.");

    return 0;
}
