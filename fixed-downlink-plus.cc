#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("FixedDownlinkPlus");

int main(int argc, char *argv[]){
    /* Set time resolution to nanoseconds */
    Time::SetResolution(Time::NS);

    /* Enable log messages */
    LogComponentEnable("FixedDownlinkPlus", LOG_INFO); 

    /* Enable checksums */
    GlobalValue::Bind("ChecksumEnabled", BooleanValue(true));

    /* Create the Nodes */
    NS_LOG_INFO("Creating the Nodes...");
    NodeContainer wifiApNodes;
    wifiApNodes.Create(1);
    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(1);

    /* Create the wifi channel */
    NS_LOG_INFO("Setting up the channel...");
    WifiHelper wifi;
    wifi.SetStandard(WIFI_PHY_STANDARD_80211n_2_4GHZ);
    wifi.SetRemoteStationManager("ns3::MinstrelHtPlusWifiManager");

    YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default();
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();

    MobilityHelper apMobility;
    apMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    Ptr<ListPositionAllocator> apPositionAlloc = CreateObject<ListPositionAllocator>();
    apPositionAlloc->Add(Vector(0.0, 0.0, 1.0));
    apMobility.SetPositionAllocator(apPositionAlloc);
    apMobility.Install(wifiApNodes);

    MobilityHelper staMobility;
    staMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    Ptr<ListPositionAllocator> staPositionAlloc = CreateObject<ListPositionAllocator>();
    staPositionAlloc->Add(Vector(50.0, 0.0, 1.0));
    staMobility.SetPositionAllocator(staPositionAlloc);
    staMobility.Install(wifiStaNodes);

    wifiPhy.SetChannel(wifiChannel.Create());

    WifiMacHelper wifiMac;
    Ssid ssid = Ssid("ns3");

    wifiMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiApDevices = wifi.Install(wifiPhy, wifiMac, wifiApNodes);

    wifiMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer wifiStaDevices = wifi.Install(wifiPhy, wifiMac, wifiStaNodes);

    /* Install the internet stack */
    NS_LOG_INFO("Installing the internet stack...");
    InternetStackHelper internet;
    internet.Install(wifiApNodes);
    internet.Install(wifiStaNodes);

    /* Assign the IP addresses */
    NS_LOG_INFO("Assigning the IP addresses...");
    Ipv4AddressHelper address;
    address.SetBase("192.168.1.0", "255.255.255.0");
    Ipv4InterfaceContainer wifiApInterfaces = address.Assign(wifiApDevices);
    Ipv4InterfaceContainer wifiStaInterfaces = address.Assign(wifiStaDevices);

    /* Setup the applications */
    NS_LOG_INFO("Install the applications...");
    uint16_t port = 9;
    PacketSinkHelper sink("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    ApplicationContainer apps_sink = sink.Install(wifiStaNodes.Get(0));
    apps_sink.Start(Seconds(0.5));
    apps_sink.Stop(Seconds(100.0));

    OnOffHelper onoff("ns3::UdpSocketFactory", InetSocketAddress(wifiStaInterfaces.GetAddress(0), port));
    onoff.SetConstantRate(DataRate("400Mb/s"), 1420);
    onoff.SetAttribute("StartTime", TimeValue(Seconds(0.5)));
    onoff.SetAttribute("StopTime", TimeValue(Seconds(100.0)));
    ApplicationContainer apps_source = onoff.Install(wifiApNodes.Get(0));

    /* Set PCAP attributes */
    //Config::SetDefault("ns3::PcapFileWrapper:NanosecMode", BooleanValue(true));
    wifiPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
    wifiPhy.EnablePcap("ap", wifiApDevices.Get(0));
    wifiPhy.EnablePcap("sta", wifiStaDevices.Get(0));

    /* Start simulation */
    NS_LOG_INFO("Running simulation...");
    Simulator::Stop(Seconds(100.0));
    Simulator::Run();
    Simulator::Destroy();
    NS_LOG_INFO("Finished simulation.");

    return 0;
}